<%@ page import="java.util.List" %>
<%@ page import="com.example.apphotelbooking.entity.Application" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My Orders</title>
    <style>

        table {
            margin: auto;
        }

        th {
            font-weight: bold;
            text-align: left;
            background-color: #ccc;
            padding: 5px 10px;
        }

        td {
            padding: 5px 10px;
        }
    </style>
</head>
<body>

<% List<Application> applicationList = (List<Application>) request.getAttribute("applicationList"); %>
<% if (applicationList == null || applicationList.isEmpty()) {%>
<div style="color: green; text-align: center;">Your Applications are not found</div>
<%} else {%>
<h1 style="text-align: center;">Applications</h1>
<% for (Application app : applicationList) { %>
<table>
    <tr>
        <th>From Date</th>
        <td><%=app.getFromDate()%>
        </td>
    </tr>
    <tr>
        <th>To Date</th>
        <td><%=app.getToDate()%>
        </td>
    </tr>
    <tr>
        <th>Total Price</th>
        <td><%=app.getTotalPrice()%>
        </td>
    </tr>
    <tr>
        <th>Application status</th>
        <td><%=app.getStatus()%>
        </td>
    </tr>
    <tr>
        <th>Submitted Date</th>
        <td><%=app.getSubmittedTime()%>
        </td>
    </tr>
</table>
<% } %>
<% } %>
</body>
</html>