<%--
  Created by IntelliJ IDEA.
  User: jalol
  Date: 8/15/2023
  Time: 7:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Room</title>
</head>
<style>
    form {
        display: flex;
        flex-direction: column;
        max-width: 400px;
        margin: 0 auto;
    }

    input[type="number"],
    input[type="text"],
    select,
    textarea {
        margin-bottom: 10px;
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 4px;
        font-size: 16px;
    }

    input[type="number"]:focus,
    input[type="text"]:focus,
    select:focus,
    textarea:focus {
        outline: none;
        border-color: #4d90fe;
    }

    input[type="submit"] {
        background-color: #4d90fe;
        color: #fff;
        border: none;
        padding: 10px 20px;
        border-radius: 4px;
        cursor: pointer;
        font-size: 16px;
    }

    input[type="submit"]:hover {
        background-color: #357ae8;
    }
</style>
<body>
  <form action="add_room" method="post">
      <input type="number" name="room number" min="0" placeholder="room number" required>
      <input type="number" name="floor number" min="0" placeholder="floor number" required>
      <input type="text" name="cost" min="0" placeholder="cost per night" required>
      <select name="room type" id="room type">
          <option value="single">Single</option>
          <option value="double">Double</option>
          <option value="suite">Suite</option>
      </select>
      <textarea name="description">Description</textarea>
      <input type="number" name="bed number" min="0" placeholder="number of beds" required>
      <input type="submit" value="Submit">
  </form>
</body>
</html>
