<%--
  Created by IntelliJ IDEA.
  User: jalol
  Date: 7/22/2023
  Time: 3:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>Registration Page</title>
  <style>
    body {
      font-family: Arial, sans-serif;
      background-color: #f2f2f2;
    }

    .container {
      margin: auto;
      width: 50%;
      padding: 20px;
      background-color: #fff;
      border-radius: 5px;
      box-shadow: 0px 0px 10px #888888;
    }

    h2 {
      text-align: center;
    }

    input[type=text], input[type=password] {
      width: 100%;
      padding: 12px 20px;
      margin: 8px 0;
      display: inline-block;
      border: 1px solid #ccc;
      border-radius: 4px;
      box-sizing: border-box;
    }

    button {
      background-color: #4CAF50;
      color: white;
      padding: 14px 20px;
      margin: 8px 0;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      width: 100%;
    }

    button:hover {
      background-color: #45a049;
    }

    .link {
      text-align: center;
      margin-top: 20px;
    }

    .link a {
      color: #4CAF50;
      text-decoration: none;
    }

    .link a:hover {
      text-decoration: underline;
    }
  </style>
</head>
<body>
<div class="container">
  <h2>Hotel Booking Registration Fomr</h2>
  <c:if test="${not empty errorMessage}">
    <p style="color: red">${errorMessage}</p>
  </c:if>

  <form action="register" method="post">
    <label for="firstname">Firstname:</label>
    <input type="text" name="firstname" id="firstname" required placeholder="Enter your firstname"><br>
    <label for="lastname">Lastname:</label>
    <input type="text" name="lastname" id="lastname" required placeholder="Enter your lastname"><br>
    <label for="email">Email:</label>
    <input type="text" name="email" id="email" required placeholder="Enter your email"><br>
    <label for="phoneNumber">Phone Number:</label>
    <input type="text" name="phoneNumber" id="phoneNumber" required placeholder="(00)0000000" pattern="^\d{9}$"><br>
    <label for="password">Password:</label>
    <input type="password" name="password" id="password" required placeholder="Enter your password"><br>
    <label for="confirmPassword">Confirm password:</label>
    <input type="password" name="confirm password" id="confirmPassword" required placeholder="Confirm your password"><br>
    <button type="submit">Register</button>
  </form>
  <div class="link">
    <p>Already have an account? <a href="index.jsp">Login</a></p>
  </div>
</div>
</body>
</html>
