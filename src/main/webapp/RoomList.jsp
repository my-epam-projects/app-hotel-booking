<%@ page import="java.util.List" %>
<%@ page import="com.example.apphotelbooking.entity.Room" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Rooms</title>
    <style>
        table {
            margin: auto;
        }

        th {
            font-weight: bold;
            text-align: left;
            background-color: #ccc;
            padding: 5px 10px;
        }

        td {
            padding: 5px 10px;
        }
    </style>
</head>
<body>
<%
    List<Room> roomList = (List<Room>) request.getAttribute("roomList");
    if (roomList == null || roomList.isEmpty()) {
%>
<div style="color: green; text-align: center;">Rooms not found</div>
<%
} else {
%>
<h1 style="text-align: center;">Rooms</h1>
<%
    for (int i = 0; i < roomList.size(); i++) {
        Room room = roomList.get(i);
%>
<table>
    <tr>
        <th>Room Number</th>
        <td><%= room.getRoomNumber() %></td>
    </tr>
    <tr>
        <th>Floor Number</th>
        <td><%= room.getFloorNumber() %></td>
    </tr>
    <tr>
        <th>Cost per night</th>
        <td><%= room.getCost() %></td>
    </tr>
    <tr>
        <th>Room Type</th>
        <td><%= room.getRoomType() %></td>
    </tr>
    <tr>
        <th>Description</th>
        <td><%= room.getDescription() %></td>
    </tr>
    <tr>
        <th>Number of Beds</th>
        <td><%= room.getBedNumber() %></td>
    </tr>
</table>
<% if (i < roomList.size() - 1) { %>
<hr>
<% } %>
<%
        }
    }
%>
</body>
</html>