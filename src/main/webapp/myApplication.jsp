<%@ page import="com.example.apphotelbooking.entity.Application" %>
<%@ page import="com.example.apphotelbooking.entity.Room" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>My Application</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            font-size: 16px;
            line-height: 24px;
            background-color: #f2f2f2;
        }

        h1 {
            color: #333;
            font-size: 36px;
            font-weight: bold;
            margin: 0 0 20px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 20px;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            background-color: #4CAF50;
            color: #fff;
            font-weight: bold;
        }

        .center {
            text-align: center;
        }
    </style>
</head>
<body>
<h1>Your Application</h1>
<% Application app = (Application) request.getSession().getAttribute("application"); %>
<% Room room = (Room) request.getSession().getAttribute("room"); %>
<table>
    <tr>
        <th>Room Type</th>
        <th><%=room.getRoomType()%>
        </th>
    </tr>
    <tr>
        <td>Room cost per night</td>
        <td><%=room.getCost()%>
        </td>
    </tr>
    <tr>
        <td>Room Floor number</td>
        <td><%=room.getFloorNumber()%>
        </td>
    </tr>
    <tr>
        <td>Room Number of Beds</td>
        <td><%=room.getBedNumber()%>
        </td>
    </tr>
    <tr>
        <td>Checkin Date</td>
        <td><%=app.getFromDate()%>
        </td>
    </tr>
    <tr>
        <td>Checkout Date</td>
        <td><%=app.getToDate()%>
        </td>
    </tr>
    <tr>
        <td>Total price</td>
        <td><%=app.getTotalPrice()%>
        </td>
    </tr>
</table>

<form method="post" action="booking">
    <div class="center">
        <button type="submit" name="book" value="book">Book</button>
        <button type="submit" name="cancel" value="cancel">Cancel</button>
    </div>
</form>
</body>
</html>