<%@ page import="java.util.List" %>
<%@ page import="com.example.apphotelbooking.entity.Application" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Applications</title>
    <style>

        table {
            margin: auto;
        }

        th {
            font-weight: bold;
            text-align: left;
            background-color: #ccc;
            padding: 5px 10px;
        }

        td {
            padding: 5px 10px;
        }
    </style>
</head>
<body>
<% List<Application> applicationList = (List<Application>) request.getAttribute("applicationList"); %>
<% if (applicationList == null || applicationList.isEmpty()) {%>
<div style="color: green; text-align: center;">Applications not found</div>
<%} else {%>
<h1 style="text-align: center;">Applications</h1>
<% for (Application app : applicationList) { %>
<table>
    <tr>
        <th>Client fullName</th>
        <td><%=app.getUser().getFirstname() + " " + app.getUser().getLastname()%>
        </td>
    </tr>
    <tr>
        <th>User Email</th>
        <td><%=app.getUser().getEmail()%>
        </td>
    </tr>
    <tr>
        <th>User Phone Number</th>
        <td><%=app.getUser().getPhone()%>
        </td>
    </tr>
    <tr>
        <th>Room Number</th>
        <td><%=app.getRoom().getRoomNumber()%>
        </td>
    </tr>
    <tr>
        <th>Room Type</th>
        <td><%=app.getRoom().getRoomType()%>
        </td>
    </tr>
    <tr>
        <th>Number of Beds</th>
        <td><%=app.getRoom().getBedNumber()%>
        </td>
    </tr>
    <tr>
        <th>From Date</th>
        <td><%=app.getFromDate()%>
        </td>
    </tr>
    <tr>
        <th>To Date</th>
        <td><%=app.getToDate()%>
        </td>
    </tr>
    <tr>
        <th>Total Price</th>
        <td><%=app.getTotalPrice()%>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;">
            <form method="post" action="admin">
                <input type="hidden" name="app_id" value="<%=app.getId()%>">
                <input type="submit" name="action" value="Confirm">
                <input type="submit" name="action" value="Reject">
            </form>
        </td>
    </tr>
</table>
<% } %>
<% } %>
</body>
</html>