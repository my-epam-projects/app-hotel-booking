<!DOCTYPE html>
<html>
<head>
    <title>Hotel Booking Form</title>
    <style>
        h1 {
            color: #333;
            font-size: 36px;
            font-weight: bold;
            margin: 0 0 20px;
        }

        form {
            font-size: 16px;
            line-height: 24px;
            margin-bottom: 20px;
        }

        label {
            display: block;
            font-weight: bold;
            margin-bottom: 10px;
        }

        select, input[type="date"], input[type="submit"] {
            font-size: 16px;
            padding: 8px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            width: 100%;
            margin-bottom: 20px;
        }

        input[type="submit"] {
            background-color: #4CAF50;
            color: #fff;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #3e8e41;
        }

        p.error {
            color: red;
            font-weight: bold;
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
<h1>Hotel Booking Form</h1>


<c:if test="${not empty errorMessage}">
    <p class="error">${errorMessage}</p>
</c:if>

<form action="search_room" method="get">
    <label for="beds">Number of Beds:</label>
    <select name="beds" id="beds">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
    </select>

    <label for="room type">Room Type:</label>
    <select name="room type" id="room type">
        <option value="single">Single</option>
        <option value="double">Double</option>
        <option value="suite">Suite</option>
    </select>

    <label for="checkin">Check-in Date:</label>
    <input type="date" name="checkin" id="checkin" required>

    <label for="checkout">Check-out Date:</label>
    <input type="date" name="checkout" id="checkout" required>

    <input type="submit" value="Book">
</form>
</body>
</html>