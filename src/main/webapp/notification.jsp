<%@ page import="java.util.List" %>
<%@ page import="com.example.apphotelbooking.entity.Application" %>
<%@ page import="com.example.apphotelbooking.entity.Notification" %>
<%@ page import="com.example.apphotelbooking.entity.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Applications</title>
    <style>

        table {
            margin: auto;
        }

        th {
            font-weight: bold;
            text-align: left;
            background-color: #ccc;
            padding: 5px 10px;
        }

        td {
            padding: 5px 10px;
        }
    </style>
</head>
<body>
<% List<Notification> notificationList = (List<Notification>) request.getAttribute("notificationList"); %>
<% User user = (User) request.getAttribute("user"); %>
<% List<Application> applicationList = (List<Application>) request.getAttribute("applicationList"); %>
<% if (notificationList == null || notificationList.isEmpty()) {%>
<div style="color: green; text-align: center;">Notifications are not found</div>
<%} else {%>
<h1 style="text-align: center;">Notifications</h1>
<% for (Notification notification : notificationList) { %>
<% for (Application app : applicationList) { %>
<% if (app.getId().equals(notification.getApplicationId())) {%>
<table>
    <tr>
        <th>User</th>
        <td><%=user.getFirstname() + " " + user.getLastname()%>
        </td>
    </tr>
    <tr>
        <th>User Email</th>
        <td><%=user.getEmail()%>
        </td>
    </tr>
    <tr>
        <th>User Phone Number</th>
        <td><%=user.getPhone()%>
        </td>
    </tr>
    <tr>
        <th>From Date</th>
        <td><%=app.getFromDate()%>
        </td>
    </tr>
    <tr>
        <th>To Date</th>
        <td><%=app.getToDate()%>
        </td>
    </tr>
    <tr>
        <th>Total Price</th>
        <td><%=app.getTotalPrice()%>
        </td>
    </tr>
    <tr>
        <th>Application status</th>
        <td><%=app.getStatus()%>
        </td>
    </tr>
    <tr>
        <th>Notification description</th>
        <td><%=notification.getDesc()%>
        </td>
    </tr>
    <tr>
        <th>Notification Date</th>
        <td><%=notification.getCreatedAt()%>
        </td>
    </tr>
</table>
<% } %>
<% } %>
<% } %>
<% } %>
</body>
</html>