<%@ page import="com.example.apphotelbooking.entity.Room" %>
<%@ page import="java.util.List" %>
<html>
<head>
    <title>Available Rooms</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        h1 {
            text-align: center;
            margin-top: 20px;
        }

        form {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        input[type="radio"] {
            margin: 10px;
        }

        label {
            display: block;
            margin: 10px;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            cursor: pointer;
        }

        label:hover {
            background-color: #f2f2f2;
        }

        input[type="submit"] {
            padding: 10px;
            background-color: #008CBA;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            margin-top: 20px;
        }

        input[type="submit"]:hover {
            background-color: #006B8F;
        }
    </style>
</head>
<body>
<h1>Available Rooms</h1>
<% List<Room> availableRooms = (List<Room>)request.getAttribute("availableRooms"); %>

<form action="booking" method="get">
    <% for (Room room : availableRooms) { %>
    <input type="radio" id="<%= room.getId() %>" name="roomId" value="<%= room.getId() %>">
    <label for="<%= room.getId() %>">Room number - <%= room.getRoomNumber() %><br>
        Room type - <%= room.getRoomType() %><br>
        Floor number - <%= room.getFloorNumber()%><br>
        Price - <%= room.getCost() %>USD<br>
        Number of Beds - <%= room.getBedNumber() %><br>
        Description - <%= room.getDescription() %></label>
    <% } %>
    <input type="submit" value="Book Room">
</form>
</body>
</html>