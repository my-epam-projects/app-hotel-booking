<!DOCTYPE html>
<html>
<head>
    <title>User Domain</title>
    <style>
        /* Header */
        header {
            background-color: #f2f2f2;
            padding: 20px;
            text-align: center;
        }

        /* Navigation */
        nav {
            background-color: #333;
            color: #fff;
            padding: 10px;
        }

        nav ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        nav ul li {
            display: inline;
        }

        nav ul li a {
            color: #fff;
            padding: 10px;
            text-decoration: none;
        }

        nav ul li a:hover {
            background-color: #555;
        }

    </style>
</head>
<body>
<nav>
    <ul>
        <li><a href="orders">My Orders</a></li>
        <li><a href="notification">Notifications</a></li>
        <li><a href="checkRoom.jsp">Book</a></li>
    </ul>
</nav>
</body>
</html>