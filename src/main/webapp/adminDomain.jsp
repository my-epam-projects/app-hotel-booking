<%--
  Created by IntelliJ IDEA.
  User: jalol
  Date: 8/15/2023
  Time: 7:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Admin domain</title>

</head>
<style>

    body, h1, h2, h3, h4, h5, h6, p,
    ul, ol, li, nav {
        margin: 0;
        padding: 0;
    }


    header {
        background-color: #333;
        color: #fff;
        padding: 27px;
        margin: 7px 18px;
    }


    nav ul {
        list-style-type: none;
        background-color: #f2f2f2;
        padding: 10px;
    }

    nav ul li {
        display: inline;
        margin-right: 10px;
    }

    nav ul li a {
        text-decoration: none;
        color: #333;
        padding: 5px 10px;
    }

    nav ul li a:hover {
        background-color: #333;
        color: #fff;
    }

    #content {
        padding: 20px;
    }
</style>
<body>
<header>
    <h1>User domain</h1>
</header>
<nav>
    <ul>
        <li><a href="admin">Applications</a></li>
        <li><a href="room.jsp">Manage Rooms</a></li>
    </ul>
</nav>
</body>
</html>
