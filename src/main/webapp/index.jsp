<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
        }

        .container {
            margin: auto;
            width: 50%;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0px 0px 10px #888888;
        }

        h2 {
            text-align: center;
        }

        input[type=text], input[type=password] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        button {
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            width: 100%;
        }

        button:hover {
            background-color: #45a049;
        }

        .link {
            text-align: center;
            margin-top: 20px;
        }

        .link a {
            color: #4CAF50;
            text-decoration: none;
        }

        .link a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
<div class="container">
    <h2>Hotel Booking Login Form</h2>
    <c:if test="${not empty message}">
        <p style="color: green">${message}</p>
    </c:if>

    <c:if test="${not empty errorMessage}">
        <p style="color: red">${errorMessage}</p>
    </c:if>
    <form action="login" method="post">
        <label for="email">Email</label>
        <input type="text" id="email" name="email" placeholder="Enter your email">

        <label for="password">Password</label>
        <input type="password" id="password" name="password" placeholder="Enter your password">

        <button type="submit">Login</button>
    </form>
    <div class="link">
        <p>Don't have an account? <a href="register.jsp">Register</a></p>
    </div>
</div>
</body>
</html>