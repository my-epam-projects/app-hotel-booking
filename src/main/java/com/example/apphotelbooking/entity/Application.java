package com.example.apphotelbooking.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Application {
    private Long id;
    private User user;
    private Integer userId;
    private LocalDate fromDate;
    private LocalDate toDate;
    private Room room;
    private Integer roomId;
    private Float totalPrice;
    private String status;
    private LocalDateTime submittedTime;

    public Application(Long id, Integer userId, LocalDate fromDate, LocalDate toDate, Integer roomId, Float totalPrice, String status, LocalDateTime submittedTime) {
        this.id = id;
        this.userId = userId;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.roomId = roomId;
        this.totalPrice = totalPrice;
        this.status = status;
        this.submittedTime = submittedTime;
    }

    public Application(Integer userId, LocalDate fromDate, LocalDate toDate, Room room) {
        this.userId = userId;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.room = room;
    }
}
