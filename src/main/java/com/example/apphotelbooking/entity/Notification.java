package com.example.apphotelbooking.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Notification {
    private Integer id;

    private Integer userId;
    private String desc;
    private boolean isRead;
    private Long applicationId;
    private LocalDateTime createdAt;

    public Notification(Integer userId, String desc, boolean isRead, Long applicationId, LocalDateTime createdAt) {
        this.userId = userId;
        this.desc = desc;
        this.isRead = isRead;
        this.applicationId = applicationId;
        this.createdAt = createdAt;
    }
}
