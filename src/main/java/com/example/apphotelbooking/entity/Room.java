package com.example.apphotelbooking.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Room {
    private Integer id;
    private Integer roomNumber;
    private Integer floorNumber;
    private Double cost;
    private String roomType;
    private String description;
    private Integer bedNumber;


    public Room(Integer id, Integer roomNumber, Integer floorNumber, Double cost, String roomType, String description, Integer bedNumber) {
        this.id = id;
        this.roomNumber = roomNumber;
        this.floorNumber = floorNumber;
        this.cost = cost;
        this.roomType = roomType;
        this.description = description;
        this.bedNumber = bedNumber;
    }

    public Room(Integer roomNumber, Integer floorNumber, Double cost, String roomType, String description, Integer bedNumber) {
        this.roomNumber = roomNumber;
        this.floorNumber = floorNumber;
        this.cost = cost;
        this.roomType = roomType;
        this.description = description;
        this.bedNumber = bedNumber;
    }
}
