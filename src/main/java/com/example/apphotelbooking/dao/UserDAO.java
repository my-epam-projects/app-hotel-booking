package com.example.apphotelbooking.dao;

import com.example.apphotelbooking.entity.User;

public interface UserDAO extends Dao<User, Integer> {

    User isValidUser(String email, String password);

    boolean isValidEmail(String email);

    boolean isExistEmail(String email);

}
