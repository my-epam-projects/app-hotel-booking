package com.example.apphotelbooking.dao;

import com.example.apphotelbooking.entity.Room;

import java.util.List;
import java.util.Optional;

public interface RoomDAO extends Dao<Room,Integer> {

    @Override
    Optional<Room> getById(Integer Id);

    @Override
    List<Room> getAll();

    @Override
    boolean save(Room room);

    @Override
    boolean delete(Integer roomId);

    List<Room> getAvailableRooms(String checkin, String checkout, String roomType, Integer bedNumber);
}
