package com.example.apphotelbooking.dao;

import com.example.apphotelbooking.entity.Application;
import com.example.apphotelbooking.entity.Notification;
import com.example.apphotelbooking.entity.Room;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ApplicationDAO extends Dao<Application, Integer> {
    @Override
    Optional<Application> getById(Integer Id);

    @Override
    List<Application> getAll();

    @Override
    boolean save(Application application);

    @Override
    boolean delete(Integer applicationId);

    boolean update(Integer appId, String status);

    List<Application> getApplications();

    List<Integer> getRoomIds(String checkin, String checkout);


    List<Application> getAppsByNotifications(List<Notification> notificationList);
    List<Application> getByUser(Integer userId);
}
