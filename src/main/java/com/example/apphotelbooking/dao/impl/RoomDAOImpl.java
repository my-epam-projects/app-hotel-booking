package com.example.apphotelbooking.dao.impl;

import com.example.apphotelbooking.config.Connector;
import com.example.apphotelbooking.dao.ApplicationDAO;
import com.example.apphotelbooking.dao.RoomDAO;
import com.example.apphotelbooking.entity.Room;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class RoomDAOImpl implements RoomDAO {

    private final ApplicationDAO applicationDAO;

    public RoomDAOImpl(ApplicationDAO applicationDAO) {
        this.applicationDAO = applicationDAO;
    }


    @Override
    public Optional<Room> getById(Integer id) {
        final String query = "SELECT * FROM rooms t WHERE t.id = ?";
        try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(this.toRoom(resultSet));
            }
            resultSet.close();
            return Optional.empty();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Room> getAll() {
        List<Room> roomList = new ArrayList<>();
        final String query = "select * from rooms";
        try (Statement statement = Connector.getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                roomList.add(this.toRoom(resultSet));
            }
            return roomList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean save(Room room) {
        final String query = "insert into rooms(room_number, floor_number, cost, room_type, description, bed_number) " +
                "VALUES(?,?,?,?,?,?) ";
        try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
            statement.setInt(1, room.getRoomNumber());
            statement.setInt(2, room.getFloorNumber());
            statement.setDouble(3, room.getCost());
            statement.setString(4, room.getRoomType());
            statement.setString(5, room.getDescription());
            statement.setInt(6, room.getBedNumber());

            return statement.executeUpdate() > 0;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean delete(Integer roomId) {
        final String query = "delete from rooms where id = ?";
        try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
            statement.setInt(1, roomId);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Room> getAvailableRooms(String checkin, String checkout, String roomType, Integer bedNumber) {
        List<Integer> roomIds = applicationDAO.getRoomIds(checkin, checkout);
        if (roomIds.isEmpty()) {
            final String query = "SELECT * FROM rooms t WHERE t.room_type = ? AND t.bed_number = ?";
            try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
                statement.setString(1, roomType);
                statement.setInt(2, bedNumber);

                ResultSet rs = statement.executeQuery();

                return this.getRooms(rs);
            } catch (SQLException e) {
                if (e.getMessage().startsWith("ERROR: operator does not exist:")) return new ArrayList<>();
                throw new RuntimeException(e);
            }
        } else {
            StringBuilder builder = new StringBuilder();

            builder.append("?,".repeat(roomIds.size()));
            String placeHolders = builder.deleteCharAt(builder.length() - 1).toString();
            final String query = "select * from rooms t where t.id not in (" + placeHolders + ")" +
                    "and t.room_type = ? AND t.bed_number = ?";

            try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
                int index = 1;
                for (Object o : roomIds) {
                    statement.setObject(index++, o);
                }
                statement.setString(index++, roomType);
                statement.setInt(index, bedNumber);
                ResultSet resultSet = statement.executeQuery();
                return this.getRooms(resultSet);
            } catch (SQLException e) {
                if (e.getMessage().startsWith("ERROR: operator does not exist:")) return new ArrayList<>();
                throw new RuntimeException(e);
            }
        }
    }


    private List<Room> getRooms(ResultSet resultSet) throws SQLException {
        List<Room> roomList = new ArrayList<>();
        while (resultSet.next()) {
            roomList.add(this.toRoom(resultSet));
        }
        resultSet.close();
        return roomList;
    }

    private Room toRoom(ResultSet resultSet) {
        try {
            Integer id = resultSet.getInt("id");
            Integer roomNumber = resultSet.getInt("room_number");
            Integer floorNumber = resultSet.getInt("floor_number");
            Double cost = resultSet.getDouble("cost");
            String roomType = resultSet.getString("room_type");
            String desc = resultSet.getString("description");
            Integer bedNumber = resultSet.getInt("bed_number");
            return new Room(id, roomNumber, floorNumber, cost, roomType, desc, bedNumber);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
