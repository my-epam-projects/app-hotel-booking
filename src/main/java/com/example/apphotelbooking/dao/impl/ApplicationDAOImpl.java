package com.example.apphotelbooking.dao.impl;

import com.example.apphotelbooking.config.Connector;
import com.example.apphotelbooking.dao.ApplicationDAO;
import com.example.apphotelbooking.entity.Application;
import com.example.apphotelbooking.entity.Notification;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ApplicationDAOImpl implements ApplicationDAO {

    public boolean update(Integer appId, String status) {
        final String query = "update applications set status = ? where id = ?";
        try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
            statement.setString(1, status);
            statement.setInt(2, appId);
            int rowsAffected = statement.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Application> getById(Integer appId) {
        final String query = "select * from applications where id = ?";
        try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
            statement.setInt(1, appId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(this.toApplication(resultSet));
            }
            resultSet.close();
            return Optional.empty();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Application> getApplications() {
        List<Application> applicationList = new ArrayList<>();
        final String query = "select * from applications where status = 'UNDER REVIEW' order by submitted_time";
        return getApplications(applicationList, query);
    }

    private List<Application> getApplications(List<Application> applicationList, String query) {
        try (Statement statement = Connector.getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                applicationList.add(this.toApplication(resultSet));
            }
            resultSet.close();
            return applicationList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // todo fix connector

    public boolean save(Application app) {
        final String query = "insert into applications(from_date, to_date, room_id , total_price, status, submitted_time, user_id)" +
                " VALUES \n" +
                " (?,?,?,?,?,?,?);";
        try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
            statement.setDate(1, Date.valueOf(app.getFromDate()));
            statement.setDate(2, Date.valueOf(app.getToDate()));
            statement.setInt(3, app.getRoom().getId());
            long gap = Math.abs(ChronoUnit.DAYS.between(app.getFromDate(), app.getToDate()));
            statement.setFloat(4, (float) (app.getRoom().getCost() * gap));
            statement.setString(5, "UNDER REVIEW");
            statement.setTimestamp(6, Timestamp.valueOf(LocalDateTime.now()));
            statement.setInt(7, app.getUserId());

            int result = statement.executeUpdate();
            return result > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Application toApplication(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong("id");
        LocalDate fromDate = resultSet.getDate("from_date").toLocalDate();
        LocalDate toDate = resultSet.getDate("to_date").toLocalDate();
        Integer roomId = resultSet.getInt("room_id");
        Float totalPrice = resultSet.getFloat("total_price");
        String status = resultSet.getString("status");
        LocalDateTime submittedTime = resultSet.getTimestamp("submitted_time").toLocalDateTime();
        Integer userId = resultSet.getInt("user_id");
        return new Application(id, userId, fromDate, toDate, roomId, totalPrice, status, submittedTime);
    }

    public List<Integer> getRoomIds(String checkin, String checkout) {

        final String query = "select t.room_id\n" +
                "from applications t\n" +
                "WHERE (\n" +
                "        (t.from_date <= ? AND t.to_date >= ?) \n" +
                "        OR (t.from_date >= ? AND t.from_date <= ?) \n" +
                "        OR (t.to_date >= ? AND t.to_date <= ?)\n" +
                "    )";
        List<Integer> roomIds = new ArrayList<>();
        try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
            statement.setDate(1, Date.valueOf(checkin));
            statement.setDate(2, Date.valueOf(checkout));
            statement.setDate(3, Date.valueOf(checkin));
            statement.setDate(4, Date.valueOf(checkout));
            statement.setDate(5, Date.valueOf(checkin));
            statement.setDate(6, Date.valueOf(checkout));
            ResultSet resultSet = statement.executeQuery();


            while (resultSet.next()) {
                roomIds.add(resultSet.getInt("room_id"));
            }
            resultSet.close();
            return roomIds;
        } catch (SQLException e) {
            if (e.getMessage().startsWith("ERROR: operator does not exist:"))
                return roomIds;
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Application> getAppsByNotifications(List<Notification> notificationList) {
        List<Long> ids = notificationList.stream().map(Notification::getApplicationId).collect(Collectors.toList());
        List<Application> applicationList = new ArrayList<>();
        if (!ids.isEmpty()) {
            StringBuilder builder = new StringBuilder();
            builder.append("?,".repeat(ids.size()));
            String placeHolders = builder.deleteCharAt(builder.length() - 1).toString();
            final String query = "select * from applications where id in (" + placeHolders + ")";

            try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
                int index = 1;
                for (Long l : ids) {
                    statement.setLong(index++, l);
                }
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    applicationList.add(this.toApplication(resultSet));
                }
                resultSet.close();
                return applicationList;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return applicationList;
    }


    @Override
    public List<Application> getByUser(Integer userId) {
        final String query = "select * from applications where user_id = ?";
        List<Application> applicationList = new ArrayList<>();
        try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                applicationList.add(this.toApplication(resultSet));
            }
            resultSet.close();
            return applicationList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Application> getAll() {
        final String query = "select * from applications";
        List<Application> applicationList = new ArrayList<>();
        return getApplications(applicationList, query);
    }


    @Override
    public boolean delete(Integer applicationId) {
        final String query = "delete from applications where id = ?";
        try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
            statement.setInt(1, applicationId);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
