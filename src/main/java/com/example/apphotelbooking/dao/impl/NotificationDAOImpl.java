package com.example.apphotelbooking.dao.impl;

import com.example.apphotelbooking.config.Connector;
import com.example.apphotelbooking.dao.NotificationDAO;
import com.example.apphotelbooking.entity.Notification;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class NotificationDAOImpl implements NotificationDAO {
    @Override
    public Optional<Notification> getById(Integer Id) {
        final String query = "select * from notifications where id = ?";
        try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
            statement.setInt(1, Id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(this.toNotification(resultSet));
            }
            return Optional.empty();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Notification> getAll() {
        final String query = "select * from notifications";
        List<Notification> notificationList = new ArrayList<>();
        try (Statement statement = Connector.getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                notificationList.add(this.toNotification(resultSet));
            }
            return notificationList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean save(Notification notification) {
        final String query = "insert into notifications(user_id, description, is_read, application_id,created_at) values (?,?,?,?,?)";
        try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
            statement.setInt(1, notification.getUserId());
            statement.setString(2, notification.getDesc());
            statement.setBoolean(3, false);
            statement.setLong(4, notification.getApplicationId());
            statement.setTimestamp(5, Timestamp.valueOf(LocalDateTime.now()));
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean delete(Integer t) {
        final String query = "delete from notifications where id = ?";
        try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Notification> getByUser(Integer userId) {
        final String query = "select * from notifications t where t.user_id = ?";
        List<Notification> notificationList = new ArrayList<>();
        try (PreparedStatement statement = Connector.getConnection().prepareStatement(query)) {
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                notificationList.add(this.toNotification(resultSet));
            }
            return notificationList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Notification toNotification(ResultSet resultSet) throws SQLException {
        Integer userId = resultSet.getInt("user_id");
        String description = resultSet.getString("description");
        boolean isRead = resultSet.getBoolean("is_read");
        Long appId = resultSet.getLong("application_id");
        LocalDateTime createdAt = resultSet.getTimestamp("created_at").toLocalDateTime();
        return new Notification(userId, description, isRead, appId, createdAt);
    }
}
