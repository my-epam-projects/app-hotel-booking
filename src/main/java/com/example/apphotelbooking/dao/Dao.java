package com.example.apphotelbooking.dao;

import java.util.List;
import java.util.Optional;

public interface Dao<T,Id> {
    Optional<T> getById(Id Id);

    List<T> getAll();

    boolean save(T t);

    boolean delete(Id t);
}
