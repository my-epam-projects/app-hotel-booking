package com.example.apphotelbooking.dao;

import com.example.apphotelbooking.entity.Notification;

import java.util.List;

public interface NotificationDAO extends Dao<Notification,Integer> {

    @Override
    boolean save(Notification notification);

    List<Notification> getByUser(Integer userId);
}
