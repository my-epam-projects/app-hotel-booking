package com.example.apphotelbooking.servlets;

import com.example.apphotelbooking.dao.ApplicationDAO;
import com.example.apphotelbooking.dao.RoomDAO;
import com.example.apphotelbooking.dao.impl.ApplicationDAOImpl;
import com.example.apphotelbooking.dao.impl.RoomDAOImpl;
import com.example.apphotelbooking.entity.Room;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(urlPatterns = "/add_room")
public class AddRoomServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer roomNumber = Integer.parseInt(request.getParameter("room number"));
        Integer floorNumber = Integer.parseInt(request.getParameter("floor number"));
        Double cost = Double.parseDouble(request.getParameter("cost"));
        String roomType = request.getParameter("room type");
        String description = request.getParameter("description");
        Integer bedNumber = Integer.parseInt(request.getParameter("bed number"));


        Room room = new Room(roomNumber, floorNumber, cost, roomType, description, bedNumber);
        ApplicationDAO dao = new ApplicationDAOImpl();
        RoomDAO roomDAO = new RoomDAOImpl(dao);
        if (roomDAO.save(room)) {
            response.setStatus(HttpServletResponse.SC_CREATED);
            String message = "<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                    "    <title>Document</title>\n" +
                    "</head><body>   <p align=\"center\" style=\"color: green;\">Room joined successfully!!!</p>\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>";
            response.getWriter().write(message);
        } else {
            response.sendError(HttpServletResponse.SC_CONFLICT, "Cannot save room try again");
        }
    }
}