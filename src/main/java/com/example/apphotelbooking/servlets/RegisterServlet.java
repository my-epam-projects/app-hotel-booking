package com.example.apphotelbooking.servlets;

import com.example.apphotelbooking.dao.impl.UserDAOImpl;
import com.example.apphotelbooking.entity.User;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstname = req.getParameter("firstname");
        String lastname = req.getParameter("lastname");
        String phone = req.getParameter("phoneNumber");
        String password = req.getParameter("password");
        String email = req.getParameter("email");
        String confirmPassword = req.getParameter("confirm password");

        if (!password.equals(confirmPassword)) {
            this.forwardWithError(req, resp, "Passwords did not match!!!");
            return;
        }

        UserDAOImpl dao = new UserDAOImpl();
        if (!dao.isValidEmail(email)) {
            this.forwardWithError(req, resp, "Invalid email address!!!");
            return;
        }

        if (dao.isExistEmail(email)) {
            this.forwardWithError(req, resp, "Email address already exists!!!");
            return;
        }

        boolean isRegistered = dao.save(new User(firstname,lastname,email,password,phone,"ROLE_ADMIN"));
        if (isRegistered) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("index.jsp");
            req.setAttribute("message", "Registered successfully");
            dispatcher.forward(req, resp);
        }
        this.forwardWithError(req, resp, "cannot register please try again!!!");
    }

    private void forwardWithError(HttpServletRequest req, HttpServletResponse resp, String errorMessage) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("register.jsp");
        req.setAttribute("errorMessage", errorMessage);
        dispatcher.forward(req, resp);
    }
}
