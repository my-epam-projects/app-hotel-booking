package com.example.apphotelbooking.servlets;

import com.example.apphotelbooking.dao.impl.UserDAOImpl;
import com.example.apphotelbooking.entity.User;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("index.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        UserDAOImpl dao = new UserDAOImpl();
        User user = dao.isValidUser(email, password);
        if (user == null) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("index.jsp");
            req.setAttribute("errorMessage", "email or password is incorrect!!!");
            dispatcher.forward(req, resp);
            return;
        }

        req.getSession().setAttribute("user", user);
        req.getSession().setAttribute("role", user.getRole());

        RequestDispatcher dispatcher;
        if (user.getRole().equals("ROLE_ADMIN")) {
            dispatcher = req.getRequestDispatcher("adminDomain.jsp");
        } else {
            dispatcher = req.getRequestDispatcher("domain.jsp");
        }
        dispatcher.forward(req, resp);
    }
}
