package com.example.apphotelbooking.servlets;

import com.example.apphotelbooking.dao.ApplicationDAO;
import com.example.apphotelbooking.dao.NotificationDAO;
import com.example.apphotelbooking.dao.impl.ApplicationDAOImpl;
import com.example.apphotelbooking.dao.impl.NotificationDAOImpl;
import com.example.apphotelbooking.entity.Application;
import com.example.apphotelbooking.entity.Notification;
import com.example.apphotelbooking.entity.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet("/notification")
public class NotificationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");
        NotificationDAO dao = new NotificationDAOImpl();
        ApplicationDAO applicationDAO = new ApplicationDAOImpl();
        List<Notification> notificationList = dao.getByUser(user.getId());
        List<Application> applicationList = applicationDAO.getAppsByNotifications(notificationList);
        req.setAttribute("notificationList", notificationList);
        req.setAttribute("user", user);
        req.setAttribute("applicationList", applicationList);
        req.getRequestDispatcher("notification.jsp").forward(req, resp);
        super.doGet(req, resp);
    }
}
