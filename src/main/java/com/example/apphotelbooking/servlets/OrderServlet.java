package com.example.apphotelbooking.servlets;

import com.example.apphotelbooking.dao.ApplicationDAO;
import com.example.apphotelbooking.dao.impl.ApplicationDAOImpl;
import com.example.apphotelbooking.entity.Application;
import com.example.apphotelbooking.entity.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/orders")
public class OrderServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ApplicationDAO dao = new ApplicationDAOImpl();
        User user = (User) req.getSession().getAttribute("user");
        List<Application> applicationList = dao.getByUser(user.getId());
        req.setAttribute("applicationList", applicationList);
        req.getRequestDispatcher("myorders.jsp").forward(req, resp);
    }
}
