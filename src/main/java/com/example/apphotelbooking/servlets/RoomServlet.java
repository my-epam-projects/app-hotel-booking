package com.example.apphotelbooking.servlets;

import com.example.apphotelbooking.dao.ApplicationDAO;
import com.example.apphotelbooking.dao.RoomDAO;
import com.example.apphotelbooking.dao.impl.ApplicationDAOImpl;
import com.example.apphotelbooking.dao.impl.RoomDAOImpl;
import com.example.apphotelbooking.entity.Room;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/room")
public class RoomServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ApplicationDAO dao = new ApplicationDAOImpl();
        RoomDAO roomDAO = new RoomDAOImpl(dao);

        List<Room> roomList = roomDAO.getAll();
        if (roomList.isEmpty()) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "rooms are not found");
        }
        req.setAttribute("roomList", roomList);
        RequestDispatcher dispatcher = req.getRequestDispatcher("RoomList.jsp");
        dispatcher.forward(req, resp);
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        if (action != null) {
            if (action.equals("rooms")) {
                this.doGet(req, resp);
            } else if (action.equals("add new room")) {
                RequestDispatcher dispatcher = req.getRequestDispatcher("addRoom.jsp");
                dispatcher.forward(req, resp);
            } else {
                resp.sendRedirect("delete_room");
            }
        }
    }

}
