package com.example.apphotelbooking.servlets;

import com.example.apphotelbooking.dao.impl.ApplicationDAOImpl;
import com.example.apphotelbooking.dao.impl.RoomDAOImpl;
import com.example.apphotelbooking.entity.Application;
import com.example.apphotelbooking.entity.Room;
import com.example.apphotelbooking.entity.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@WebServlet(urlPatterns = "/booking")
public class BookingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int roomId = Integer.parseInt(req.getParameter("roomId"));
        String checkin = (String) req.getSession().getAttribute("checkin");
        String checkout = (String) req.getSession().getAttribute("checkout");

        long gap = ChronoUnit.DAYS.between(LocalDate.parse(checkin), LocalDate.parse(checkout));
        ApplicationDAOImpl dao = new ApplicationDAOImpl();
        RoomDAOImpl roomDAO = new RoomDAOImpl(dao);
        Optional<Room> room = roomDAO.getById(roomId);
        if (room.isEmpty()){
            resp.sendError(HttpServletResponse.SC_NOT_FOUND,"cannot find room");
            return;
        }
        Float totalPrice = (float) (room.get().getCost() * gap);
        Application application = new Application();
        application.setFromDate(LocalDate.parse(checkin));
        application.setToDate(LocalDate.parse(checkout));
        application.setTotalPrice(totalPrice);

        req.getSession().setAttribute("application", application);
        req.getSession().setAttribute("room", room.get());

        req.getRequestDispatcher("myApplication.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bookButton = req.getParameter("book");
        String cancelButton = req.getParameter("cancel");
        if (bookButton != null) {
            Application application = (Application) req.getSession().getAttribute("application");
            Room room = (Room) req.getSession().getAttribute("room");
            User user = (User) req.getSession().getAttribute("user");
            ApplicationDAOImpl dao = new ApplicationDAOImpl();
            Application app = new Application(user.getId(), application.getFromDate(),application.getToDate(),room);
            boolean isSaved = dao.save(app);
            if (isSaved) {
                resp.setStatus(HttpServletResponse.SC_CREATED);

                String message = "<!DOCTYPE html>\n" +
                        "<html lang=\"en\">\n" +
                        "<head>\n" +
                        "    <meta charset=\"UTF-8\">\n" +
                        "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                        "    <title>Document</title>\n" +
                        "</head><body>   <p align=\"center\" style=\"color: green;\">Successfully booked, wait invoice from admin,invoice will be sent to your phone number</p>\n" +
                        "\n" +
                        "</body>\n" +
                        "</html>";
                resp.getWriter().write(message);
                return;
            }
            resp.sendError(HttpServletResponse.SC_CONFLICT, "Cannot save application try again");
        } else if (cancelButton != null) {
            req.getRequestDispatcher("checkRoom.jsp").forward(req, resp);
        }
    }
}
