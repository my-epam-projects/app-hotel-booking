package com.example.apphotelbooking.servlets;

import com.example.apphotelbooking.dao.ApplicationDAO;
import com.example.apphotelbooking.dao.RoomDAO;
import com.example.apphotelbooking.dao.impl.ApplicationDAOImpl;
import com.example.apphotelbooking.dao.impl.RoomDAOImpl;
import com.example.apphotelbooking.entity.Room;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/delete_room")
public class DeleteRoomServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ApplicationDAO dao = new ApplicationDAOImpl();
        RoomDAO roomDAO = new RoomDAOImpl(dao);

       List<Room> roomList = roomDAO.getAll();
       if (roomList.isEmpty()){
           response.sendError(HttpServletResponse.SC_NOT_FOUND,"rooms are not found");
           return;
       }
       request.setAttribute("roomList",roomList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("removeRoom.jsp");
        dispatcher.forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer roomId = Integer.valueOf(req.getParameter("roomId"));
        ApplicationDAO dao = new ApplicationDAOImpl();
        RoomDAO roomDAO = new RoomDAOImpl(dao);
        if (roomDAO.delete(roomId)){
            resp.setStatus(HttpServletResponse.SC_OK);
            String message = "<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                    "    <title>Document</title>\n" +
                    "</head><body>   <p align=\"center\" style=\"color: green;\">Deleted success" +
                    "fully</p>\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>";
            resp.getWriter().write(message);
            return;
        }
        resp.sendError(HttpServletResponse.SC_CONFLICT,"cannot delete room");
    }
}