package com.example.apphotelbooking.servlets;

import com.example.apphotelbooking.dao.ApplicationDAO;
import com.example.apphotelbooking.dao.NotificationDAO;
import com.example.apphotelbooking.dao.RoomDAO;
import com.example.apphotelbooking.dao.UserDAO;
import com.example.apphotelbooking.dao.impl.ApplicationDAOImpl;
import com.example.apphotelbooking.dao.impl.NotificationDAOImpl;
import com.example.apphotelbooking.dao.impl.RoomDAOImpl;
import com.example.apphotelbooking.dao.impl.UserDAOImpl;
import com.example.apphotelbooking.entity.Application;
import com.example.apphotelbooking.entity.Notification;
import com.example.apphotelbooking.entity.Room;
import com.example.apphotelbooking.entity.User;
import com.example.apphotelbooking.service.SendInvoiceService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet(urlPatterns = "/admin")
public class AdminServlet extends HttpServlet {

    //    add history and crud in rooms and applications
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ApplicationDAOImpl dao = new ApplicationDAOImpl();
        RoomDAO roomDAO = new RoomDAOImpl(dao);
        UserDAO userDAO = new UserDAOImpl();
        List<Application> applicationList = dao.getApplications();
        for (Application application : applicationList) {
            Optional<User> user = userDAO.getById(application.getUserId());
            Optional<Room> room = roomDAO.getById(application.getRoomId());
            if (user.isEmpty()) {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Cannot find user");
                return;
            } else if (room.isEmpty()) {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Cannot find room");
                return;
            }
            application.setRoom(room.get());
            application.setUser(user.get());
        }

        req.setAttribute("applicationList", applicationList);
        RequestDispatcher dispatcher = req.getRequestDispatcher("confirmApps.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");

        Integer appId = Integer.parseInt(req.getParameter("app_id"));
        ApplicationDAO dao = new ApplicationDAOImpl();
        UserDAOImpl userDAO = new UserDAOImpl();
        Optional<Application> application = dao.getById(appId);

        if (application.isEmpty()) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Cannot find application");
            return;
        }
        Optional<User> user = userDAO.getById(application.get().getUserId());
        SendInvoiceService service = new SendInvoiceService();
        if (user.isEmpty()) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Cannot find user");
            return;
        }

        String sms = "";
        if (action.equals("Confirm")) {
            boolean isUpdated = updateApplicationStatus(appId, "CONFIRMED", dao);
            if (isUpdated) {
                sms = "your booking was confirmed successfully, We will wait you in our hotel and we hope you will enjoy";
                this.sendInvoiceAndRespond(service, user.get().getEmail(), sms, resp);
            } else {
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "cannot update application status!!!");
            }
        } else if (action.equals("Reject")) {
            boolean isUpdated = updateApplicationStatus(appId, "REJECTED", dao);
            if (isUpdated) {
                sms = "your booking was rejected, please do not hesitate and book room again";
                this.sendInvoiceAndRespond(service, user.get().getEmail(), sms, resp);
            } else {
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "cannot update application status!!!");
            }
        }
        NotificationDAO nDao = new NotificationDAOImpl();
        Notification notification = new Notification();
        notification.setUserId(application.get().getUserId());
        notification.setApplicationId(application.get().getId());
        notification.setDesc(sms);
        nDao.save(notification);
    }

    private void sendInvoiceAndRespond(SendInvoiceService service, String email, String sms, HttpServletResponse resp) throws IOException {
        try {
            if (service.sendInvoice(sms, email, "jaloliddinov010@gmail.com")) {
                resp.setStatus(HttpServletResponse.SC_OK);
                resp.getWriter().write("invoice sent successfully");
            } else {
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "cannot send invoice!!!");
            }
        } catch (MessagingException e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "cannot send invoice!!!");
        }
    }

    private boolean updateApplicationStatus(Integer appId, String status, ApplicationDAO dao) throws IOException {
        return dao.update(appId, status);
    }
}
