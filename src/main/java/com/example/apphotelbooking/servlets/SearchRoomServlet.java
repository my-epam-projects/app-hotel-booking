package com.example.apphotelbooking.servlets;


import com.example.apphotelbooking.dao.RoomDAO;
import com.example.apphotelbooking.dao.impl.ApplicationDAOImpl;
import com.example.apphotelbooking.dao.impl.RoomDAOImpl;
import com.example.apphotelbooking.entity.Room;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;


@WebServlet(urlPatterns = "/search_room")
public class SearchRoomServlet extends HttpServlet {

    //    todo add history too
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int beds = Integer.parseInt(req.getParameter("beds"));
        String roomType = req.getParameter("room type");
        String checkIn = req.getParameter("checkin");
        String checkOut = req.getParameter("checkout");

        ApplicationDAOImpl applicationDAO = new ApplicationDAOImpl();
        RoomDAO dao = new RoomDAOImpl(applicationDAO);
        if (LocalDate.parse(checkIn).isAfter(LocalDate.parse(checkOut))) {
            req.setAttribute("errorMessage", "incorrect date, please enter correct date");
            RequestDispatcher dispatcher = req.getRequestDispatcher("checkRoom.jsp");
            dispatcher.forward(req, resp);
            return;
        }

        List<Room> availableRooms = dao.getAvailableRooms(checkIn, checkOut, roomType, beds);

        if (availableRooms.isEmpty()) {
            req.setAttribute("errorMessage", "rooms are not available");
            RequestDispatcher dispatcher = req.getRequestDispatcher("checkRoom.jsp");
            dispatcher.forward(req, resp);
            return;
        }
        req.setAttribute("availableRooms", availableRooms);
        req.getSession().setAttribute("checkin", checkIn);
        req.getSession().setAttribute("checkout", checkOut);
        RequestDispatcher dispatcher = req.getRequestDispatcher("available_rooms.jsp");
        dispatcher.forward(req, resp);
    }
}
