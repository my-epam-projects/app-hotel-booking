package com.example.apphotelbooking.config;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebFilter(urlPatterns = "/*")
public class AuthenticationFilter implements Filter {

    public void init(FilterConfig config) throws ServletException {
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);

        String path = request.getRequestURI();
        if (path.equals("/app_hotel_booking_war_exploded/")) {
            chain.doFilter(req, res);
            return;
        }

        if (path.endsWith("/login") || path.endsWith("/register.jsp")|| path.endsWith("/register")) {
            chain.doFilter(req, res);
        } else if (session == null || session.getAttribute("user") == null) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        } else {
            chain.doFilter(req, res);
        }
    }

    public void destroy() {
    }
}