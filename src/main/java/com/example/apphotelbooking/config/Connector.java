package com.example.apphotelbooking.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {
    public static Connection getConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            final String url = "jdbc:postgresql://localhost:5432/hotel_booking";
            final String dbUser = "postgres";
            final String dbPassword = "postgres";

            return DriverManager.getConnection(url, dbUser, dbPassword);

        } catch (ClassNotFoundException e) {
            System.out.println("Driver error!");
            throw new RuntimeException(e);
        } catch (SQLException e) {
            System.out.println("Database connection error!");
            throw new RuntimeException(e);
        }
    }
}
